from __future__ import print_function

import time

import cv2
import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow.keras import Model
from tensorflow.keras.layers import Conv2D, Dense


class SimpleNet(Model):
    def __init__(self, num_classes=10):
        super(SimpleNet, self).__init__()
        self.num_classes = num_classes
        # TODO: Initialize the layers of your network
        # You can find different layers in tensorflow.keras.layers (https://www.tensorflow.org/versions/r2.0/api_docs/python/tf/keras/layers)
        self.conv1 = Conv2D(6, 5, strides=2, activation=tf.nn.relu, padding='valid')
        self.conv2 = Conv2D(16, 5, strides=2, activation=tf.nn.relu, padding='valid')
        self.fc1 = Dense(120, activation=tf.nn.relu)
        self.fc2 = Dense(84, activation=tf.nn.relu)
        self.fc3 = Dense(10)

    def call(self, x, visualise=False):
        # TODO: Run the image through your network
        # Your input should be a [Batch_size x 3 x 32 x 32] sized tensor
        # Your output should be a [Batch_size x 10] sized matrix
        x = tf.cast(x, tf.float32)
        x = self.conv1(x)
        x = self.conv2(x)

        if visualise:
            # TODO: Visualize a layer in your network with cv2.imshow
            show_layer = x[0, :, :, 0].numpy()

            #normalize between 0 and 255
            show_layer /= show_layer.max()
            show_layer = (255*show_layer).astype(np.uint8)
            show_img = cv2.resize(show_layer, (512, 512))
            cv2.imshow("conv2", show_img)
            cv2.waitKey(10)

        x = tf.reshape(x, [x.shape[0], -1])
        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)
        # Return the result of your network
        return x


def train():
    # Create a dataset
    ds_train, ds_test = tfds.load('cifar10', split=['train', 'test'])
    ds_train = ds_train.shuffle(200).batch(16).prefetch(10)
    ds_test = ds_test.batch(32)

    # Create network 
    net = SimpleNet()

    # Create loss function and optimizer
    criterion = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
    optimizer = tf.keras.optimizers.Adam()

    #Run training
    for epoch in range(2):
        running_loss = 0.0
        tic = time.time()

        for i, features in enumerate(ds_train):
            inputs = features['image']
            labels = features['label']

            # run the network and save gradients
            should_visualize = i % 100 == 0
            with tf.GradientTape() as tape:
                outputs = net(inputs, should_visualize)
                loss = criterion(labels, outputs)

            # Fetch gradient, and run update step
            gradients = tape.gradient(loss, net.trainable_variables)
            optimizer.apply_gradients(zip(gradients, net.trainable_variables))

            # print statistics
            running_loss += loss.numpy()
            if i % 200 == 199:  # print every 200 mini-batches
                print('[%d, %5d] loss: %.3f, time: %.3f' %
                      (epoch + 1, i + 1, running_loss / 200, time.time() - tic))
                running_loss = 0.0
                tic = time.time()
            if i % 2000 == 1999:  # test every 2000 mini-batches
                test_losses = []
                test_accuracies = []
                for features in ds_test:
                    inputs_test = features['image']
                    labels_test = features['label']
                    
                    test_output = net(inputs_test)
                    test_loss = criterion(labels_test, test_output).numpy()
                    
                    predicted_label = tf.argmax(test_output, -1)
                    correct_guess = tf.equal(predicted_label, labels_test)
                    test_acc = tf.cast(correct_guess, tf.double).numpy().mean()
                    
                    test_losses.append(test_losses)
                    test_accuracies.append(test_acc)
                print('TEST [%d, %5d] loss: %.3f, acc: %.3f' %
                      (epoch + 1, i + 1, np.mean(test_loss), np.mean(test_acc)))


if __name__ == '__main__':
    train()
