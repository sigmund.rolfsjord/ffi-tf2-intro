from __future__ import print_function
import random

import cv2
import numpy as np
import tensorflow as tf
from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input


class LiveDataset:
    def __init__(self):
        self.train_set = {}

    def add_image(self, image, label):
        train_image = self.convert_to_tf_image(image)
        if label in self.train_set:
            self.train_set[label] += [train_image]
        else:
            self.train_set[label] = [train_image]

    def get_batch(self, batch_size=16):
        labels = []
        images = []
        for i in range(batch_size):
            labels += [np.random.choice(list(self.train_set.keys()))]
            images += random.sample(self.train_set[labels[-1]], 1)
        return np.stack(images), np.stack(labels)

    @staticmethod
    def convert_to_tf_image(image):
        train_image = cv2.resize(cv2.cvtColor(image, cv2.COLOR_BGR2RGB), (224, 224))
        train_image = tf.cast(train_image, tf.float32)
        train_image = preprocess_input(train_image)
        return train_image


def run_live():
    net = MobileNetV2(weights='imagenet', include_top=True)
    for l in net.layers[:-1]:
        l.trainable = False

    criterion = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
    optimizer = tf.keras.optimizers.Adam()

    dataset = LiveDataset()
    train = False
    cnt = 0

    cap = cv2.VideoCapture(0)
    ret = True

    while ret:
        ret, image = cap.read()

        output = net(LiveDataset.convert_to_tf_image(image)[tf.newaxis])
        output = tf.argmax(output, -1).numpy()[0]
        image = cv2.putText(image, 'label: ' + chr(output), (10, 100), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 50, 50), 3)
        cv2.imshow("img", image)
        key = cv2.waitKey(1)

        if key == ord(' '):
            train = not train

        if not train:
            if key == -1:
                continue

            label = key
            dataset.add_image(image, label)
        else:
            inputs, labels = dataset.get_batch()


            with tf.GradientTape() as tape:
                outputs = net(inputs)
                loss = criterion(labels, outputs)
            gradients = tape.gradient(loss, net.trainable_variables)
            optimizer.apply_gradients(zip(gradients, net.trainable_variables))
            print('[%d] loss: %.3f' %
                  (cnt, loss))
            cnt += 1
            for i in range(3):
                print('WAIT', cap.grab())


if __name__ == '__main__':
    run_live()
