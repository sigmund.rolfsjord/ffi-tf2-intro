# Tensorflow 2 intro
For this exercise you need python installed. If you don’t have python you can install it here: https://docs.conda.io/en/latest/miniconda.html

You also need the python packages: opencv-python, torch, torchvision.
They can be installed by running:

	pip install -r requirements.txt

from the base folder, or running:

	pip install opencv-python tensorflow-gpu==2.0.0-beta1 tensorflow-datasets

from anywhere. More information on how to install torch can be found here https://pytorch.org/get-started/locally/. Here you can choose among different versions of CUDA or operating systems.

This lab is a set of exercises to learn the basics for training and running deep neural networks with python and tensorflow 2. The lab is split in four exercises: EX1_simple_network.py, EX2_finetune.py, EX3_segmentation.py and EX4_live_training.py.

Go through the exercise files and solve the comments marked with TODO.

## EX1_simple_network.py
In this exercise you should implement the SimpleNet class, to run a small network for classification of the cifar10 dataset. The cifar10 dataset consist of very small images from 10 different categories.

The forward method is called every time you run your network. This function should transform your input image, through a series of convolutions, into a vector of scores for each class.



It is important to remember that you cannot initialize your layers in the forward method, since then you will create new weight for each run and not learn anything. Use the __init__ method to initialize your layers.

Try to visualize how some the output of your layers, with cv2.imshow. Visualize both the image and the convolution result side by side, and investigate what the different layers does.

To fetch the images from tensorflow you can do: x.numpy()

## EX2_finetune.py
In this exercises you are still supposed to classify the cifar10 dataset, but you should leverage that someone already trained a network to classify ImageNet. With:

    net = MobileNetV2(weights='imagenet', include_top=False) from tensorflow.keras.applications

If you only want to use part of the model, you can make a new model based on the old with for e.g:

    new_net = Model(inputs=net.input, outputs=\[net.get_layer("out_relu").output\])

You can fetch as many inputs as you want.

For fast training you should only train the last layer of you network. You can add a last layer after the output of your base network. To ensures that you don’t spend computational resources on calculating unnecessary gradients, you need to set trainable to false for the layers you are not training.
For example:

    net.trainable = False
    
or:

    for l in net.layers[:-5]:
        l.trainable = False


Since the network initially are trained with 224x224 sized images, we will probably get better result by still using this size. We therefore resize our image appropriately. We also divide our image by 255, to get closer to the image type, we trained with.

#### Additional challenge
Try to use the logging tool called `tensorboard` to visualize the loss and accuracy.

## EX3_segmentation.py
In this exercise you are building a network for road segmentation. In other words you are trying to classify every pixel in the image.
A simple way to do this, is to use only convolutional layers, and not any Dense/Fully-connected layers.

#### Loss function
Try to implement your own loss function. The simplest solution is probably to use *mean squared distance*. If you are doing multi-class segmentation you are probably better of using *softmax cross-entropy*, but remember to do softmax over the right axis.

#### Visualization
Try to visualize the road overlayed on the input image. You can either use *tensorboard* or *cv2.imshow*.

#### Additional challenge
Try using a pretrained network as base, with multiple outputs, for example as described in the [LinkNet paper](https://codeac29.github.io/projects/linknet/).

## EX4_live_training.py
In this exercise you should finetune a network with training images generated live from a web-camera.

We have already implemented a class named LiveDataset, where we can add images and extract training batches.

Create a training loop that adds images to the dataset, and train the last layer of the network to learn the classes we input.

#### Additional challenge
Try to train a siamese network to separate the images, for example as described in [Signature Verification using a "Siamese"
Time Delay Neural Network](https://papers.nips.cc/paper/769-signature-verification-using-a-siamese-time-delay-neural-network.pdf) or
[Using Siamese CNNs for Removing Duplicate Entries From Real-Estate Listing
Databases](http://cs231n.stanford.edu/reports/2017/pdfs/7.pdf)

