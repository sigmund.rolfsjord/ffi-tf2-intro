from __future__ import print_function

import glob
import os

import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Conv2D
import cv2
import numpy as np


def preprocess(image, segmentation):
    """
    A preprocess function the is run after images are read. Here you can do augmentation and other
    processesing on the images.
    """

    # Set images size to a constant
    image_size = [224, 224]
    image = tf.image.resize(image, image_size)
    segmentation = tf.image.resize(segmentation, image_size, method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    image = tf.cast(image, tf.float32) / 255
    segmentation = tf.cast(segmentation, tf.int64)

    # Do some processing

    return image, segmentation


def read_image_and_segmentation(img_f, seg_f):
    """
    Read images from file using tensorflow and convert the segmentation to appropriate formate.
    :param img_f: filename for image
    :param seg_f: filename for segmentation
    :return: Image and segmentation tensors
    """
    img_reader = tf.io.read_file(img_f)
    seg_reader = tf.io.read_file(seg_f)
    img = tf.image.decode_png(img_reader, channels=3)
    seg = tf.image.decode_png(seg_reader)[:, :, 2:]
    seg = tf.where(seg > 0, tf.ones_like(seg), tf.zeros_like(seg))
    return img, seg


def kitti_generator_from_filenames(image_names, segmentation_names, preprocess=preprocess, batch_size=8):
    """
    Convert a list of filenames to tensorflow images.
    :param image_names: image filenames
    :param segmentation_names: segmentation filenames
    :param preprocess: A function that is run after the images are read, the takes image and
    segmentation as input
    :param batch_size: The batch size returned from the function
    :return: Tensors with images and corresponding segmentations
    """
    dataset = tf.data.Dataset.from_tensor_slices(
        (image_names, segmentation_names)
    )

    dataset = dataset.shuffle(buffer_size=100)
    dataset = dataset.map(read_image_and_segmentation)
    dataset = dataset.map(preprocess)
    dataset = dataset.batch(batch_size)

    return dataset


def kitti_image_filenames(dataset_folder, training=True):
    sub_dataset = 'training' if training else 'testing'
    segmentation_names = glob.glob(os.path.join(dataset_folder, sub_dataset, 'gt_image_2', '*road*.png'),
                                   recursive=True)
    image_names = [f.replace('gt_image_2', 'image_2').replace('_road_', '_') for f in segmentation_names]
    return image_names, segmentation_names


class SmallSegmentationNet(Model):
    def __init__(self):
        super(SmallSegmentationNet, self).__init__()
        self.conv1 = Conv2D(32, 5, strides=3, activation=tf.nn.relu, padding='same')
        self.conv2 = Conv2D(32, 5, strides=2, activation=tf.nn.relu, padding='same')
        self.conv3 = Conv2D(1, 5, activation=None, padding='same')

    def call(self, x, **kwargs):
        in_size = x.shape
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = tf.image.resize(x, in_size[1:3])
        return x

    @staticmethod
    def loss(pred, label):
        return tf.reduce_mean((pred - tf.cast(label, tf.float32)) ** 2)


def main(_):
    model = SmallSegmentationNet()
    # Getting filenames from the kitti dataset
    image_names, segmentation_names = kitti_image_filenames('data_road')

    # Get image tensors from the filenames
    num_validation_images = 10
    train_dataset = kitti_generator_from_filenames(
        image_names[:-num_validation_images],
        segmentation_names[:-num_validation_images],
        batch_size=8)
    # Get the validation tensors
    validation_dataset = kitti_generator_from_filenames(
        image_names[-num_validation_images:],
        segmentation_names[-num_validation_images:],
        batch_size=8)

    optimizer = tf.keras.optimizers.Adam()

    # Run training
    EPOCS = 20
    for epoch in range(EPOCS):
        for images, segmentation in train_dataset:
            with tf.GradientTape() as tape:
                prediction = model(images)
                loss = model.loss(prediction, segmentation)
            predicted = tf.cast(prediction > 0.3, tf.int64)
            cv2.imshow("train", (np.concatenate((predicted[0].numpy(), segmentation[0].numpy()))*255).astype(np.uint8))
            cv2.waitKey(10)
            gradients = tape.gradient(loss, model.trainable_variables)
            optimizer.apply_gradients(zip(gradients, model.trainable_variables))
            print('loss:', loss.numpy())

        for i, (images, segmentation) in enumerate(validation_dataset):
            prediction = model(images)
            loss = model.loss(prediction, segmentation)
            predicted = tf.cast(prediction > 0.3, tf.int64)
            is_correct = tf.equal(predicted, segmentation)
            cv2.imshow("test", (np.concatenate((predicted[0].numpy(), segmentation[0].numpy()))*255).astype(np.uint8))

            cv2.waitKey(10)
            accuracy = tf.reduce_mean(tf.cast(is_correct, tf.float32))
            print('Epoch:', epoch, 'step:', i, 'loss:', loss.numpy(), 'accuracy:', accuracy.numpy())


if __name__ == '__main__':
    main(None)
