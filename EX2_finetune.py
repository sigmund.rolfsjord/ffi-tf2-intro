from __future__ import print_function

import time

import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow.keras import Model
from tensorflow.keras.layers import Conv2D, Dense
from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input, decode_predictions


class FinetuneNet(Model):
    def __init__(self, num_classes=10):
        super(FinetuneNet, self).__init__()
        self.num_classes = num_classes
        # TODO: Initialize the layers of your network
        # You can find different layers in tensorflow.keras.layers (https://www.tensorflow.org/versions/r2.0/api_docs/python/tf/keras/layers)
        mobilenet = MobileNetV2(weights='imagenet', include_top=False)
        self.base_network = Model(inputs=mobilenet.input, outputs=[mobilenet.get_layer('out_relu').output])
        self.base_network.trainable = False
        self.fc = Dense(10)

    def call(self, x):
        # TODO: Run the image through your network
        # Your input should be a [Batch_size x 3 x 32 x 32] sized tensor
        # Your output should be a [Batch_size x 10] sized matrix
        x = tf.image.resize(x, (224, 224))
        x = tf.cast(x, tf.float32)
        x = preprocess_input(x)
        x = self.run_base(x)
        # x = self.base_network(x, training=True)
        x = tf.reduce_mean(x, [1, 2])
        x = self.fc(x)
        # Return the result of your network
        return x

    @tf.function
    def run_base(self, x):
        return self.base_network(x, training=False)


def train():
    # Create a dataset
    ds_train, ds_test = tfds.load('cifar10', split=['train', 'test'])
    ds_train = ds_train.shuffle(200).batch(16).prefetch(10)
    ds_test = ds_test.batch(32)

    # Create network
    net = FinetuneNet()

    # Create loss function and optimizer
    criterion = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
    optimizer = tf.keras.optimizers.Adam(0.01)

    #Run training
    for epoch in range(2):
        running_loss = 0.0
        tic = time.time()

        for i, features in enumerate(ds_train):
            inputs = features['image']
            labels = features['label']

            # run the network and save gradients
            with tf.GradientTape() as tape:
                outputs = net(inputs)
                loss = criterion(labels, outputs)

            # Fetch gradient, and run update step
            gradients = tape.gradient(loss, net.fc.trainable_variables)
            optimizer.apply_gradients(zip(gradients, net.fc.trainable_variables))

            # print statistics
            running_loss += loss.numpy()
            if i % 20 == 19:  # print every 20 mini-batches
                print('[%d, %5d] loss: %.3f, time: %.3f' %
                      (epoch + 1, i + 1, running_loss / 20, time.time() - tic))
                running_loss = 0.0
                tic = time.time()
            if i % 200 == 199:  # test every 2000 mini-batches
                test_losses = []
                test_accuracies = []
                for features in ds_test:
                    inputs_test = features['image']
                    labels_test = features['label']

                    test_output = net(inputs_test)
                    test_loss = criterion(labels_test, test_output).numpy()

                    predicted_label = tf.argmax(test_output, -1)
                    correct_guess = tf.equal(predicted_label, labels_test)
                    test_acc = tf.cast(correct_guess, tf.double).numpy().mean()

                    test_losses.append(test_losses)
                    test_accuracies.append(test_acc)
                print('TEST [%d, %5d] loss: %.3f, acc: %.3f' %
                      (epoch + 1, i + 1, np.mean(test_loss), np.mean(test_acc)))


if __name__ == '__main__':
    train()

